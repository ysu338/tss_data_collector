import numpy as np
import pandas_market_calendars as mcal
from math import ceil
from pandas.tseries.offsets import BDay
from datetime import datetime
from util.nyse_calendar import nyse_holidays


def gen_schedule_and_early_closes(st_date, ed_date):
    nyse = mcal.get_calendar('NYSE')
    schedule = nyse.schedule(start_date=st_date, end_date=ed_date)
    early_closes_days = nyse.early_closes(schedule=schedule).index
    return schedule, early_closes_days


def gen_trd_day_of_month(date_idx):
    day_of_mo = np.zeros(date_idx.shape)
    reset, j = 1, 1

    for i in range(1, len(date_idx)):
        if date_idx[i].month != date_idx[i-1].month:
            j = i
            break

    for i in range(j, len(date_idx)):
        if date_idx[i].month != date_idx[i-1].month:
            reset = 1
        else:
            reset += 1
        day_of_mo[i] = reset

    return day_of_mo


def gen_trd_day(day, refday, mvdir):
    if mvdir == 1:
        if day.isoweekday() not in [6, 7] and day not in nyse_holidays(a=refday, b=refday + BDay(7)):
            trd_day = day
        else:
            trd_day = gen_trd_day(day + BDay(1), refday, 1)
    else:
        if day.isoweekday() not in [6, 7] and day not in nyse_holidays(a=refday - BDay(7), b=refday):
            trd_day = day
        else:
            trd_day = gen_trd_day(day - BDay(1), refday, -1)

    return trd_day


def gen_next_trd_day(ref_day):
    day = ref_day
    if day.isoweekday() not in [6, 7] and day not in nyse_holidays(a=ref_day, b=ref_day + BDay(7)):
        return day.date()
    else:
        return gen_next_trd_day(day + BDay(1))


def find_index_for_date_str(date_idx, date_str):
    if type(date_str) != str:
        raise TypeError("Input date should be a string type...")

    date_datetime = datetime.strptime(date_str, '%Y-%m-%d')
    d = date_idx.searchsorted(min(date_idx, key=lambda x: abs(x - date_datetime)))
    return d


def calc_idx_offset(date_idx1, date_idx2, d):
    """
    calculate the corresponding index for date_idx2[d] in date_idx1
    :param date_idx1: DateIndex that are longer than date_idx2
    :param date_idx2:
    :return: offset an integer
    """
    offset = date_idx1.searchsorted(date_idx2[d]) - d
    return offset


def gen_first_dt_mo_idx(df, d, date_idx):
    """
    :param df: pandas data_frame
    :param date_idx: data_frame index
    :return: idx: an integer
    """

    first_dt_mo = df.loc[str(date_idx[d].year) + '-' + str(date_idx[d].month).zfill(2), :].index[0]
    idx = date_idx.tolist().index(first_dt_mo)

    return idx


def gen_opt_anchor_indices(stidx, date_idx):
    anchor_indices = []
    anchor_dates = []

    for d in range(stidx, len(date_idx)):
        if date_idx[d].month != date_idx[d-1].month:
            anchor_dates.append(date_idx[d])
            anchor_indices.append(d)

    return anchor_indices, anchor_dates


def gen_anchor_index(d, dt_indices):

    if d - dt_indices[0] <= 10:
        fdtidx = dt_indices[0]
        return fdtidx
    elif d - dt_indices[0] > 10 and d < dt_indices[1]:
        fdtidx = dt_indices.pop(0)
        return fdtidx + 10


def week_of_month(dt):
    """
    Returns the week of the month for the specified date.
    """

    first_day = dt.replace(day=1)

    dom = dt.day
    adjusted_dom = dom + first_day.weekday()

    return int(ceil(adjusted_dom/7.0))
