import os
import ast
import shutil


def make_timezone_intra_data_dir(tss_dir, start_date, end_date, tz='utc'):
    if tz == 'utc':
        intraday_data_dir = tss_dir+start_date+'_'+end_date+'_utc/'
    else:
        intraday_data_dir = tss_dir+start_date+'_'+end_date+'/'
    gen_dir(intraday_data_dir)
    return intraday_data_dir


def read_config_file(file_path):
    with open(file_path, 'r') as inf:
        params = ast.literal_eval(inf.read())

    return params


def create_daily_storage_dir(path, univ_name):
    ts_dir = os.path.join(path, univ_name)
    univ_dir = os.path.join(ts_dir, 'Univ')
    ern_dir = os.path.join(ts_dir, 'Earnings_Date_Data')

    for path in [ts_dir, univ_dir, ern_dir]:
        gen_dir(path)
    return ts_dir, univ_dir, ern_dir


def gen_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def clear_files_in_dir(path):
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def clear_directory_and_files_inside(path):
    shutil.rmtree(path)
