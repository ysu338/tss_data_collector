import os
from util.io_util import read_config_file

# ---- source directory
cur_dir = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
cur_dir = cur_dir.replace('/config', '')
cur_dir = cur_dir.replace('/dist', '')
cur_dir = cur_dir.replace('/src', '') + '/'

# ---- local data directories
local_data_dir = cur_dir + 'data/'
input_dir = local_data_dir + 'input/'
out_dir = local_data_dir + 'output/'

# ---- readin config files
daily_config = read_config_file(input_dir+'daily_data_download_config.txt')
intraday_config = read_config_file(input_dir+'intraday_data_download_config.txt')
intra_univ_config = read_config_file(input_dir + 'tradable_universe_config.txt')

