import os
import pandas as pd
import numpy as np
from config.external_config import daily_tss_dir


def load_daily_data(group, start_yr, trade_tks):
    print("... Load Historical data for {0} ...".format(group))
    # adj and unadjusted
    fld_adj_unadj = ['PX_LAST', 'PX_OPEN']
    # adjusted
    fld_adj = []
    # unadjusted
    fld_unadj = ['PX_VOLUME', 'CUR_MKT_CAP']

    # ----------------- Original Data ------------------#
    data, hist_data_dict = {}, {}

    for fld in fld_adj_unadj:
        for tp in ['_Adj', '_UnAdj']:
            data[fld+tp] = pd.read_csv(os.path.join(daily_tss_dir, group+'_'+fld+tp+'.csv'),
                                       index_col=0, parse_dates=True).loc[start_yr:, trade_tks]
    for fld in fld_adj:
        data[fld+'_Adj'] = pd.read_csv(os.path.join(daily_tss_dir, group+'_'+fld+'_Adj.csv'),
                                       index_col=0, parse_dates=True).loc[start_yr:, trade_tks]
    for fld in fld_unadj:
        data[fld+'_UnAdj'] = pd.read_csv(os.path.join(daily_tss_dir, group+'_'+fld+'_UnAdj.csv'),
                                         index_col=0, parse_dates=True).loc[start_yr:, trade_tks]

    data_names_dict = {
        'uop': 'PX_OPEN_UnAdj', 'aop': 'PX_OPEN_Adj',
        'ucl': 'PX_LAST_UnAdj', 'acl': 'PX_LAST_Adj',
        'volume': 'PX_VOLUME_UnAdj', 'cap': 'CUR_MKT_CAP_UnAdj'
    }

    for k in data_names_dict.keys():
        hist_data_dict[k] = data[data_names_dict[k]]
        hist_data_dict[k][hist_data_dict[k].abs() < 0.001] = np.nan

    # ----------------- Derived Data ------------------#
    hist_data_dict['logrtns'] = np.log(hist_data_dict['acl']) - np.log(hist_data_dict['acl']).shift(periods=1)
    hist_data_dict['pctrtns'] = hist_data_dict['acl']/hist_data_dict['acl'].shift(periods=1) - 1
    hist_data_dict['dv'] = hist_data_dict['ucl']*hist_data_dict['volume']

    return hist_data_dict


