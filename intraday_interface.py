import logging
import pandas as pd
from shutil import copy
from util.io_util import clear_directory_and_files_inside
from config.base_config import intra_univ_config
from config.external_config import univ_name, daily_univ_dir, intra_tss_dir_utc, intra_tss_dir_local, intra_agg_dir
from config.data_config import intra_start_date, intra_end_date, intra_interval
from data.load_data import load_daily_data
from data.univ_screening import UniverseScreening
from data.daily.uni_prep import get_univ
from data.intraday.blp_interface_intraday import dl_blp_intraday_data
from data.intraday.intraday_data_univ import gen_valid_tickers_intraday, control_ticker_change
from data.intraday.intraday_data_univ import get_tickers_for_intraday_download, bbg_check_ticker_change
from data.intraday.intraday_data_univ import get_tickers_list_in_dir
from data.intraday.intraday_univ_prep import load_base_univ_info
from data.intraday.intraday_data_processing import intra_data_tz_convert_utc_to_east, merge_data_files


def gen_intraday_tradable_univ(st_date='2007-01-01'):
    print("... Generage Valid Intraday Tickers ...")
    tickers = get_univ(univ_name, daily_univ_dir)
    hist_data = load_daily_data(univ_name, st_date, tickers)
    base_univ_info = load_base_univ_info()
    UniverseScreening('intraday', **intra_univ_config).gen_signal_univ(hist_data, base_univ_info, st_date=st_date)
    valid_tickers = gen_valid_tickers_intraday(st_yr=intra_start_date[:4], min_record=0)
    valid_tickers_srs = pd.Series(valid_tickers)
    valid_tickers_srs.to_csv(daily_univ_dir+f"/Tickers_Intraday_Valid.csv", header=False)


def dl_tss_intraday():
    print("... Download Intraday Time Series Data ...")
    tickers = get_tickers_for_intraday_download(intra_tss_dir_utc)
    dl_blp_intraday_data(tickers, intra_start_date, intra_end_date, intra_tss_dir_utc, interval=intra_interval)
    intra_data_tz_convert_utc_to_east(intra_tss_dir_utc)


def clear_intra_info():
    print("... Clear Temporary Folders ...")
    clear_directory_and_files_inside(intra_tss_dir_utc)
    clear_directory_and_files_inside(intra_tss_dir_local)


def merge_data_to_agg():
    print("... Merge Downloaded Intraday Data to Aggregated Folder...")
    tks_local = get_tickers_list_in_dir(intra_tss_dir_local)
    tks_base = get_tickers_list_in_dir(intra_agg_dir)

    for tk in tks_local:
        src_path = intra_tss_dir_local + tk.replace("/", "_") + '.csv'
        des_path = intra_agg_dir + tk.replace("/", "_") + '.csv'
        if tk in tks_base:
            logging.info(f"Merge {tk} to the Aggregated Folder ...")
            merged_df = merge_data_files(src_path, des_path)
            merged_df.to_csv(des_path)
        else:
            copy(src_path, des_path)


def process_ticker_change():
    print("... Tickers Change ...")
    intra_univ_file_name = daily_univ_dir+"/Tickers_MEGAU_bics.csv"
    univ_file = pd.read_csv(intra_univ_file_name, index_col=0, header=0)

    # tickers to be changed are either in bics file or in agg folder
    bics_tickers = univ_file.index.tolist()
    tks_base = get_tickers_list_in_dir(intra_agg_dir)
    cur_tickers = sorted(list(set(bics_tickers) | set(tks_base)))

    tkch_srs = bbg_check_ticker_change(cur_tickers)
    if tkch_srs.shape[0] > 0:
        control_ticker_change(tkch_srs)


if __name__ == "__main__":
    merge_data_to_agg()
