import blpapi
import pandas as pd


def get_ref_data(tickers, fields):
    long_tickers = [str(tk) + ' US Equity' for tk in tickers]
    datadict = get_bdp_data(long_tickers, fields)
    for key in datadict:
        datadict[key] = datadict[key][long_tickers]      # make sure the order is the same
        datadict[key].index = tickers                    # use short tickers
    return datadict


def get_bbg_hist_data(tickers, fields, start_date, end_date, freq, adj="False", per_adj="ACTUAL"):
    parsed_data = blp_request(tickers, fields, start_date, end_date, freq, adj=adj, per_adj=per_adj)
    pandas_df = gen_pandas_dataframe(parsed_data)
    ans = {}
    for fld in list(set(pandas_df.columns.get_level_values(0))):
        ans[fld] = pandas_df[fld]

    return ans


def get_bdp_data(tickers, fields):
    # returns a dictionary of pandas data frame with names in fields
    pandas_dict = {}
    parsed_data = bdp_request(tickers, fields)
    df_data = pd.DataFrame.from_dict(parsed_data)

    for fld in df_data.index:
        pandas_dict[fld] = df_data.loc[fld, :]

    return pandas_dict


def gen_pandas_dataframe(parsed_data):
    pandas_dict = {}
    for tk, data in parsed_data.items():
        pandas_dict[tk] = pd.DataFrame.from_dict(data, orient='Index')

    pandas_df = pd.concat(pandas_dict, axis=1).swaplevel(i=1, j=0, axis=1)
    return pandas_df


def blp_request(tickers, fields, start_date, end_date, freq, adj="FALSE", per_adj="ACTUAL"):

    # Fill SessionOptions
    session_options = blpapi.SessionOptions()
    session_options.setServerHost('localhost')
    session_options.setServerPort(8194)

    # print "Connecting to %s:%s" % (options.host, options.port)
    # Create a Session
    session = blpapi.Session(session_options)

    # Start a Session
    if not session.start():
        print("Failed to start session.")
        return

    try:
        # Open service to get historical data from
        if not session.openService("//blp/refdata"):
            print("Failed to open //blp/refdata")
            return

        # Obtain previously opened service
        ref_data_service = session.getService("//blp/refdata")

        # Create and fill the request for the historical data
        request = ref_data_service.createRequest("HistoricalDataRequest")

        # Loop over the tickers
        for tk in tickers:
            request.getElement("securities").appendValue(tk)
        # Loop over the fields
        for fld in fields:
            request.getElement("fields").appendValue(fld)

        request.set("adjustmentFollowDPDF", "FALSE")
        request.set("adjustmentNormal", adj)
        request.set("adjustmentAbnormal", adj)
        request.set("adjustmentSplit", adj)
        request.set("periodicityAdjustment", per_adj)
        request.set("periodicitySelection", freq)
        # request.set("calendarCodeOverride", "NY")
        request.set("startDate", start_date)
        request.set("endDate", end_date)
        request.set("maxDataPoints", 100000000)

        # print "Sending Historical Data Request to Bloomberg"
        # Send the request
        session.sendRequest(request)

        parsed_data = {}

        # Process received events
        while True:
            # We provide timeout to give the chance for Ctrl+C handling:
            ev = session.nextEvent(500)
            secname, flddata = event_parser_blp(ev)

            if secname is not None:
                parsed_data[secname] = flddata

            if ev.eventType() == blpapi.Event.RESPONSE:
                # Response completly received, so we could exit
                break
    finally:
        # Stop the session
        session.stop()

    return parsed_data


def event_parser_blp(ev):
    field_dict_data = {}
    secname = None

    for msg in ev:
        try:
            security_data = msg.getElement('securityData')
            secname = security_data.getElementAsString('security')
            field_data = security_data.getElement('fieldData')
            for i in range(field_data.numValues()):
                flddict = {}
                field_element = field_data.getValueAsElement(i)
                for j in range(1, field_element.numElements()):
                    tmpstring = field_element.getElement(j).toString()
                    fldname = tmpstring[:tmpstring.find("=")-1]
                    flddict[fldname] = field_element.getElementValue(fldname)
                date = field_element.getElementValue('date')
                field_dict_data[date] = flddict
        except Exception:
            pass

    return secname, field_dict_data


def bdp_request(tickers, fields):
    # Fill SessionOptions
    session_options = blpapi.SessionOptions()
    session_options.setServerHost('localhost')
    session_options.setServerPort(8194)

    # print "Connecting to %s:%s" % (options.host, options.port)
    # Create a Session
    session = blpapi.Session(session_options)

    # Start a Session
    if not session.start():
        print("Failed to start session.")
        return
    # Open service to get historical data from
    if not session.openService("//blp/refdata"):
        print("Failed to open //blp/refdata")
        return

    # Obtain previously opened service
    ref_data_service = session.getService("//blp/refdata")

    # Create and fill the request for the historical data
    request = ref_data_service.createRequest("ReferenceDataRequest")

    # Loop over the tickers
    for tk in tickers:
        request.append("securities", tk)
    # Loop over the fields
    for fld in fields:
        request.append("fields", fld)

    # print "Sending Reference Data Request to Bloomberg ... "
    # print "Sending Request:", request
    # Send the request
    session.sendRequest(request)

    try:
        parsed_data = {}
        # Process received events
        while True:
            # We provide timeout to give the chance for Ctrl+C handling:
            ev = session.nextEvent(500)
            flddata = event_parser_bdp(ev)

            if bool(flddata):
                parsed_data.update(flddata)

            if ev.eventType() == blpapi.Event.RESPONSE:
                # Response completely received, so we could exit
                break
    finally:
        # Stop the session
        session.stop()

    return parsed_data


def event_parser_bdp(ev):
    field_data_dict = {}

    for msg in ev:
        try:
            security_data = msg.getElement('securityData')
            for k in range(security_data.numValues()):
                sec_element = security_data.getValueAsElement(k)
                secname = sec_element.getElementAsString('security')
                field_data = sec_element.getElement('fieldData')
                flddict = {}
                for j in range(field_data.numElements()):
                    tmpstring = field_data.getElement(j).toString()
                    fldname = tmpstring[:tmpstring.find("=")-1]
                    flddict[fldname] = field_data.getElementValue(fldname)
                field_data_dict[secname] = flddict
        except Exception:
            pass

    return field_data_dict


def bds_request_pseudo_bulk(tickers, fields, start_date, end_date):
    # Fill SessionOptions
    session_options = blpapi.SessionOptions()
    session_options.setServerHost('localhost')
    session_options.setServerPort(8194)

    # Create a Session
    session = blpapi.Session(session_options)

    # Start a Session
    if not session.start():
        print("Failed to start session.")
        return
    # Open service to get historical data from
    if not session.openService("//blp/refdata"):
        print("Failed to open //blp/refdata")
        return

    # Obtain previously opened service
    ref_data_service = session.getService("//blp/refdata")

    for tk in tickers:
        for fld in fields:
            request = build_request(ref_data_service, tk, fld, start_date, end_date)
            # Send the request
            session.sendRequest(request)

    evt_type = []

    try:
        parsed_data = {}
        # Process received events
        while True:
            # We provide timeout to give the chance for Ctrl+C handling:
            ev = session.nextEvent(10000)
            flddata = event_parser_bds(ev)

            evt_type.append(ev.eventType())
            if bool(flddata):
                parsed_data.update(flddata)

            if ev.eventType() == blpapi.Event.TIMEOUT:
                # Response completely received, so we could exit
                break
    finally:
        # Stop the session
        session.stop()

    return parsed_data


def build_request(ref_data_service, tk, fld, start_date, end_date):
    # Create and fill the request for the reference data
    request = ref_data_service.createRequest("ReferenceDataRequest")
    # Loop over the tickers
    request.append("securities", tk)
    # Loop over the fields
    request.append("fields", fld)
    # add overrides
    overrides = request.getElement("overrides")
    override1 = overrides.appendElement()
    override1.setElement("fieldId", "START_DT")
    override1.setElement("value", start_date)
    override2 = overrides.appendElement()
    override2.setElement("fieldId", "END_DT")
    override2.setElement("value", end_date)

    return request


def event_parser_bds(ev):
    fld_data_dict = {}

    for msg in ev:
        try:
            security_data = msg.getElement('securityData')
            for k in range(security_data.numValues()):
                sec_element = security_data.getValueAsElement(k)
                secname = sec_element.getElementAsString('security')
                field_data = sec_element.getElement('fieldData')
                flddict = {}
                field_data_zero = field_data.getElement(0)
                for i in range(field_data_zero.numValues()):
                    flddict[i] = {}
                    subvalue = field_data_zero.getValue(i)
                    for j in range(subvalue.numElements()):
                        sub_fld_name = str(subvalue.getElement(j).name())
                        flddict[i][sub_fld_name] = subvalue.getElement(j).getValue()
                fld_data_dict[secname] = flddict
        except Exception:
            pass

    return fld_data_dict
