import os
import csv
import pandas as pd


def change_tickers_in_bics_file(df_path, old_tickers, new_tickers):
    df = pd.read_csv(df_path, index_col=0, header=0, parse_dates=True)
    df.loc[old_tickers, "Tickers"] = [tk + " US Equity" for tk in new_tickers]
    df = df.rename(dict(zip(old_tickers, new_tickers)), axis='index')
    df.to_csv(df_path)


def change_tickers_in_intraday_univ_file(df_path, old_tickers, new_tickers):
    df = pd.read_csv(df_path, index_col=1, header=None, squeeze=True)
    df = df.rename(dict(zip(old_tickers, new_tickers)), axis='index')
    df = df.reset_index().drop(0, axis=1)
    df.to_csv(df_path, header=False)


def change_tickers_daily_tss_file(df_path, old_tickers, new_tickers):
    def change_tickers_in_array(arr):
        for ot, nt in zip(old_tickers, new_tickers):
            arr[arr.index(ot)] = nt
        return arr

    new_df_path = df_path.replace('.csv', '_temp.csv')

    with open(df_path, 'r') as fr, open(new_df_path, 'w', newline='') as fw:
        cr = csv.reader(fr)
        cw = csv.writer(fw)
        col_names = next(cr)
        col_names = change_tickers_in_array(col_names)
        cw.writerow(col_names)
        cw.writerows(cr)

    os.remove(df_path)
    os.rename(new_df_path, df_path)


def change_tickers_intraday_tss(path, old_tickers, new_tickers):
    for ot, nt in zip(old_tickers, new_tickers):
        try:
            os.rename(os.path.join(path, ot+'.csv'), os.path.join(path, nt+'.csv'))
        except FileNotFoundError:
            pass
