import os
import logging
import pandas as pd
from config.external_config import univ_name, daily_univ_dir, daily_ern_dir
from config. external_config import daily_tss_dir, intra_tss_dir, intra_agg_dir
from config.data_config import adj_fields, unadj_fields, intra_start_date, intra_end_date
from data.daily.blp_interface import get_ref_data
from data.intraday.intraday_data_tkch import change_tickers_in_bics_file, change_tickers_in_intraday_univ_file
from data.intraday.intraday_data_tkch import change_tickers_daily_tss_file, change_tickers_intraday_tss
from typing import List


def read_valid_tickers_intraday():
    file_name = daily_univ_dir + r"/Tickers_Intraday_Valid.csv"
    intraday_tickers_srs = pd.read_csv(file_name, index_col=0, squeeze=True, header=None)
    valid_tickers = intraday_tickers_srs.values
    return valid_tickers


def get_tickers_for_intraday_download(data_dir):
    indir_tickers = get_tickers_list_in_dir(data_dir)
    tickers = read_valid_tickers_intraday()
    tickers = sorted(list(set(tickers) - set(indir_tickers)))
    return tickers


def gen_valid_tickers_intraday(st_yr='2018-01-01', min_record=0):
    univ_df = load_signal_univ_df(start_date=st_yr)
    valid_record = univ_df[univ_df == 1].sum(0)
    sig_valid_tks = sorted(valid_record[valid_record > min_record].index.to_list())
    agg_dir_tks = get_tickers_list_in_dir(intra_agg_dir)
    valid_tks = sorted(list(set(sig_valid_tks) | set(agg_dir_tks)))
    return valid_tks


def load_signal_univ_df(start_date='2017-01-01'):
    try:
        file_name = daily_univ_dir + '/signal_univ_intraday_'+univ_name+'.csv'
        univ_df = pd.read_csv(file_name, index_col=0, parse_dates=True).loc[start_date:, :]
    except IOError:
        raise IOError("Univ_DataFrame is not ready...")
    return univ_df


def get_tickers_list_in_dir(data_dir):
    ans = list()
    for f in os.listdir(data_dir):
        if os.path.isfile(os.path.join(data_dir, f)):
            ans.append(f[:f.find('.csv')].replace("_", "/"))
    return ans


def bbg_check_ticker_change(univ_tks: List[str]) -> pd.Series:
    mkt_stat, prim_tks = "MARKET_STATUS", "EQY_PRIM_SECURITY_TICKER"
    stat_dict = get_ref_data(univ_tks, [mkt_stat, prim_tks])
    tkch_tks = stat_dict[mkt_stat][stat_dict[mkt_stat] == 'TKCH'].index
    return stat_dict[prim_tks][tkch_tks]


def control_ticker_change(tkch_srs):
    old_tickers, new_tickers = tkch_srs.index.to_list(), tkch_srs.values.tolist()
    record_ticker_change(old_tickers, new_tickers)

    # --- change tickers in universe files --- #
    bics_univ_path = daily_univ_dir + '/Tickers_' + univ_name + '_bics.csv'
    intra_univ_path = daily_univ_dir + f'/Tickers_Intraday_Valid.csv'
    change_tickers_in_bics_file(bics_univ_path, old_tickers, new_tickers)
    change_tickers_in_intraday_univ_file(intra_univ_path, old_tickers, new_tickers)

    # --- change daily tickers --- #
    tss_flds_mapper = {
        'adj': {'fields': adj_fields, 'adj_bool': 'TRUE', 'postfix': '_Adj'},
        'unadj': {'fields': unadj_fields, 'adj_bool': 'FALSE', 'postfix': '_UnAdj'}
    }
    for val in tss_flds_mapper.values():
        for key in val['fields']:
            df_path = daily_tss_dir + '/'+univ_name+'_'+key+val['postfix']+'.csv'
            change_tickers_daily_tss_file(df_path, old_tickers, new_tickers)

    # --- change ern_date_df tickers --- #
    sig_univ_path = daily_ern_dir + '/ern_date_df.csv'
    change_tickers_daily_tss_file(sig_univ_path, old_tickers, new_tickers)

    # --- change signal_univ tickers --- #
    sig_univ_path = daily_univ_dir + '/signal_univ_intraday_' + univ_name + '.csv'
    change_tickers_daily_tss_file(sig_univ_path, old_tickers, new_tickers)

    # --- change intraday tickers --- #
    print("...... Ticker Change Intraday Dates Marked Folder ......")
    data_dir = intra_tss_dir+intra_start_date+'_'+intra_end_date+'/'
    change_tickers_intraday_tss(data_dir, old_tickers, new_tickers)
    print("...... Ticker Change Intraday Agg Folder ......")
    change_tickers_intraday_tss(intra_agg_dir, old_tickers, new_tickers)
    print("...... Ticker Change Step Finished ......")


def record_ticker_change(old_tickers, new_tickers):
    for ot, nt in zip(old_tickers, new_tickers):
        logging.info(f"{ot} changed to {nt} ...")
