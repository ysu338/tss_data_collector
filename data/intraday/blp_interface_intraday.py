import logging
import blpapi
import pandas as pd
import datetime
from collections import defaultdict
from typing import List


BAR_DATA = blpapi.Name("barData")
BAR_TICK_DATA = blpapi.Name("barTickData")
OPEN = blpapi.Name("open")
HIGH = blpapi.Name("high")
LOW = blpapi.Name("low")
CLOSE = blpapi.Name("close")
VOLUME = blpapi.Name("volume")
NUM_EVENTS = blpapi.Name("numEvents")
TIME = blpapi.Name("time")
RESPONSE_ERROR = blpapi.Name("responseError")
SESSION_TERMINATED = blpapi.Name("SessionTerminated")
CATEGORY = blpapi.Name("category")
MESSAGE = blpapi.Name("message")
SERVER_PORT = 8194


def blp_intraday_request(tk, start_time, end_time, interval=5, adj="FALSE"):
    # Fill SessionOptions
    sess_options = blpapi.SessionOptions()
    sess_options.setServerHost('localhost')
    sess_options.setServerPort(SERVER_PORT)
    # Create a Session
    session = blpapi.Session(sess_options)
    # Start a Session
    if not session.start():
        print("Failed to start session.")
        return
    try:
        # Open service to get historical data from
        if not session.openService("//blp/refdata"):
            print("Failed to open //blp/refdata")
            return

        # Obtain previously opened service
        ref_data_service = session.getService("//blp/refdata")

        # Create and fill the request for the historical data
        request = ref_data_service.createRequest("IntradayBarRequest")
        request.set("eventType", "TRADE")
        request.set("interval", interval)
        # Loop over the tickers
        request.set("security", tk+" US Equity")
        request.set("startDateTime", start_time)
        request.set("endDateTime", end_time)

        request.set("adjustmentFollowDPDF", "FALSE")
        request.set("adjustmentNormal", adj)
        request.set("adjustmentAbnormal", adj)
        request.set("adjustmentSplit", adj)

        # Send the request
        session.sendRequest(request)
        # Process received events
        parsed_data = event_loop(session)
    finally:
        # Stop the session
        session.stop()

    return parsed_data


def event_loop(session):
    looped_data = {}
    done = False
    while not done:
        # nextEvent() method below is called with a timeout to let
        # the program catch Ctrl-C between arrivals of new events
        event = session.nextEvent()
        if event.eventType() == blpapi.Event.PARTIAL_RESPONSE:
            looped_data = {**looped_data, **process_response_event(event)}
        elif event.eventType() == blpapi.Event.RESPONSE:
            tmp_looped_data = process_response_event(event)
            looped_data = {**looped_data, **tmp_looped_data}
            done = True
        else:
            for msg in event:
                if event.eventType() == blpapi.Event.SESSION_STATUS:
                    if msg.messageType() == SESSION_TERMINATED:
                        done = True
    return looped_data


def process_response_event(event):
    for msg in event:
        if msg.hasElement(RESPONSE_ERROR):
            print_error_info("REQUEST FAILED: ", msg.getElement(RESPONSE_ERROR))
            continue
        return process_message(msg)


def print_error_info(lead_str, error_info):
    print("%s%s (%s)" % (lead_str, error_info.getElementAsString(CATEGORY),
                         error_info.getElementAsString(MESSAGE)))


def process_message(msg):
    data = msg.getElement(BAR_DATA).getElement(BAR_TICK_DATA)
    ans = defaultdict(dict)

    for bar in data.values():
        time = bar.getElementAsDatetime(TIME)
        ans[time]['open'] = bar.getElementAsFloat(OPEN)
        ans[time]['high'] = bar.getElementAsFloat(HIGH)
        ans[time]['low'] = bar.getElementAsFloat(LOW)
        ans[time]['close'] = bar.getElementAsFloat(CLOSE)
        ans[time]['volume'] = bar.getElementAsInteger(VOLUME)

    return ans


def dl_blp_intraday_data(tickers: List, start_date_str: str, end_date_str: str, data_dir: str, interval: int = 1):
    """
    returns a dictionary of pandas data frame with names in fields
    """
    start_date = datetime.datetime.strptime(start_date_str, "%Y%m%d")
    end_date = datetime.datetime.strptime(end_date_str, "%Y%m%d")
    start_time = datetime.datetime.combine(start_date, datetime.time(13, 30))
    end_time = datetime.datetime.combine(end_date, datetime.time(21, 30))

    for tk in tickers:
        logging.info(f"Downloading... {tk}")
        data_df = get_hist_intraday_df(tk, start_time, end_time, interval=interval)
        if data_df.shape[0] > 0:
            data_df.to_csv(data_dir + tk.replace("/", "_") + '.csv')


def get_hist_intraday_df(tk, start_time, end_time, interval=1):
    parsed_data = blp_intraday_request(tk, start_time, end_time, interval=interval)
    data_df = pd.DataFrame.from_dict(parsed_data, orient='index')
    return data_df
