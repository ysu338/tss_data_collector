import time
import logging
import pandas as pd
from pandas.tseries.offsets import BDay
from util.datetime_util import gen_trd_day
from config.external_config import daily_tss_dir
from data.daily.blp_interface import bds_request_pseudo_bulk


def dl_earnings_hist(tks, ern_dir, stdate, eddate):
    print("... Download Historical Earnings Information ...")
    tickers = [str(tk) + ' US Equity' for tk in tks]
    earn_data_raw = bds_request_pseudo_bulk(tickers, ["DY895"], stdate, eddate)

    for tk_long in tickers:
        try:
            trans_data = transform_save_earn_data(tk_long, earn_data_raw[tk_long])
        except KeyError:
            trans_data = pd.Series(0, index=[tk_long])
        tk_shrt = tk_long[:tk_long.find("US Equity") - 1]
        tk_shrt = tk_shrt.replace('/', '_')
        trans_data.to_csv(ern_dir + '/' + tk_shrt + '.csv')


def transform_save_earn_data(tk_long, sub_earn_data):
    tempdf = pd.DataFrame.from_dict(sub_earn_data, orient='index').set_index('Year/Period')
    tempdf['Fund_Data_Valid_Date'] = 0
    try:
        tempdf2 = tempdf[(tempdf['Earnings EPS'].abs() > 1e-5)]
        if tempdf2.shape[0] != 0:
            tempdf = tempdf2
        for row in tempdf.index:
            offset = gen_db_date_offset(tempdf.loc[row, 'Announcement Time'])
            # assumme bloomberg update their database 1 day after announcement date
            ann_date = tempdf.loc[row, 'Announcement Date']
            vaid_data_date = gen_trd_day(ann_date + BDay(offset), ann_date, 1)
            tempdf.loc[row, 'Fund_Data_Valid_Date'] = vaid_data_date
        tempdf = tempdf['Fund_Data_Valid_Date']
    except KeyError:
        logging.info("No earnings information for {0}".format(tk_long))

    return tempdf.copy()


def gen_db_date_offset(ann_time_str):
    offset = 0
    try:
        ann_time = time.strptime(ann_time_str, "%H:%M")
        if ann_time <= time.strptime("09:30", "%H:%M"):
            offset = 0
        elif ann_time >= time.strptime("16:00", "%H:%M"):
            offset = 1

    except ValueError:
        if ann_time_str == 'Bef_Mkt':
            offset = 0
        elif ann_time_str == 'Aft_Mkt':
            offset = 1
    return offset


def create_ern_date_df(tks, loc):
    prc = pd.read_csv(daily_tss_dir+'/SPY_PX_LAST_Adj.csv', index_col=0, header=None, parse_dates=True, squeeze=True)
    ern_date_df = pd.DataFrame(0, index=prc.index, columns=tks)
    date_index = prc.index.tolist()
    earn_data = {}
    offset = 5
    label = 100
    for tk in tks:
        try:
            tkr = tk.replace('/', '_')
            try:
                earn_data[tk] = pd.read_csv(loc+'/'+tkr+'.csv', index_col=1, parse_dates=True, squeeze=True)
                for day in earn_data[tk].index:
                    try:
                        idx = date_index.index(day)
                        for i in range(-offset, offset + 1):
                            try:
                                ern_date_df.iloc[idx + i, ern_date_df.columns.get_loc(tk)] = label + i
                            except Exception as e:
                                pass
                    except Exception as e:
                        logging.info(f'Earnings Date Exception for {tk}')
                        logging.info(e)
                        pass
            except IOError:
                pass

        except IndexError:
            logging.info(f"No Earn Ann Date Exception {tk}")
    ern_date_df.to_csv(loc + '/ern_date_df.csv')

