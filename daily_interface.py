from util.io_util import clear_files_in_dir
from config.external_config import univ_name, daily_tss_dir, daily_univ_dir, daily_ern_dir
from config.data_config import daily_start_date, daily_end_date, daily_year_range
from data.daily.eqs_interface import get_univ_thru_eqs
from data.daily.uni_prep import agg_univ_bics, get_univ
from data.daily.ern_info import dl_earnings_hist, create_ern_date_df
from data.daily.tss_info import dl_single_name_hist, dl_spy_iwm_data


def dl_univ_daily():
    get_univ_thru_eqs(univ_name, daily_year_range, daily_univ_dir)
    agg_univ_bics(univ_name, daily_year_range, daily_univ_dir)


def dl_tss_daily():
    tks = get_univ(univ_name, daily_univ_dir)
    dl_spy_iwm_data(daily_tss_dir, daily_start_date, daily_end_date)
    dl_single_name_hist(tks, daily_tss_dir, daily_start_date, daily_end_date)


def dl_ern_daily():
    tks = get_univ(univ_name, daily_univ_dir)
    dl_earnings_hist(tks, daily_ern_dir, daily_start_date, daily_end_date)
    create_ern_date_df(tks, daily_ern_dir)


def dl_all_daily():
    clear_daily_info()
    dl_univ_daily()
    dl_tss_daily()
    dl_ern_daily()


def clear_daily_info():
    for d in [daily_ern_dir, daily_univ_dir, daily_tss_dir]:
        clear_files_in_dir(d)
