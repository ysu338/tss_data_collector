import blpapi
import csv


def get_univ_thru_eqs(screen_name, yr_range, inputdir):
    print("... Running EQS to Retrieve Universe Data ...")
    for yr in yr_range:
        path_str = inputdir + '/Tickers_' + screen_name + '_' + str(yr) + '.csv'
        yrstr = str(yr) + '0101'
        tk_univ = blpeqs(screen_name, yrstr)
        tk_univ = sorted(tk_univ)

        with open(path_str, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file)
            for line in tk_univ:
                writer.writerow([line])

    return


def blpeqs(screen_name, pit_date):
    session = blpapi.Session()
    if not session.start():
        print("Failed to start session.")
    if not session.openService("//blp/refdata"):
        print("Failed to open service.")

    ref_data_service = session.getService("//blp/refdata")
    request = ref_data_service.createRequest("BeqsRequest")
    request.set("screenName", screen_name)
    request.set("screenType", "PRIVATE")

    overrides = request.getElement("overrides")
    override = overrides.appendElement()
    override.setElement("fieldId", "PiTDate")
    override.setElement("value", pit_date)

    session.sendRequest(request)

    final_response = False
    tk_univ = []

    while not final_response:
        ev = session.nextEvent(500)
        event_type = ev.eventType()
        if event_type == 5:
            tk_univ = event_parser(ev)

        if ev.eventType() == blpapi.Event.RESPONSE:
            final_response = True
            session.stop()

    return tk_univ


def event_parser(ev):
    tk_univ = []
    for msg in ev:
        try:
            data = msg.getElement('data')
            sec_data = data.getElement('securityData')
            num_elements = sec_data.numValues()
            for i in range(num_elements):
                sec_data_i = sec_data.getValue(i)
                tk = sec_data_i.getElementAsString('security').replace(' US', '')
                tk_univ.append(tk)
        except Exception:
            pass

    return tk_univ
