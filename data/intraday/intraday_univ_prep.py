import pandas as pd
from config.external_config import univ_name, daily_univ_dir


def load_base_univ_info():
    ind_lvl_name = 'Industry'
    trade_univ = pd.read_csv(daily_univ_dir + '/Tickers_' + univ_name + '_bics.csv', na_filter=False)
    trade_univ = trade_univ.set_index('Tk')

    # get rid of some non-classified tickers
    '''
    trade_univ = trade_univ[(trade_univ['Sector'] != '')
                            & (trade_univ['Sector'] != '#N/A Invalid Security')
                            & (trade_univ['Sector'] != '#N/A N/A')
                            & (trade_univ['Sector'] != 'Unclassifiable')
                            & (trade_univ['Sector'] != 'Utilities')
                            & (trade_univ['Industry'] != 'Real Estate')]
    '''
    trade_univ.sort_index(inplace=True)
    trade_tks = trade_univ.index.tolist()
    ind_lvl_ix = trade_univ.columns.to_list().index(ind_lvl_name)

    univ_info = {'trade_univ': trade_univ, 'trade_tks': trade_tks, 'ind_lvl_ix': ind_lvl_ix}

    return univ_info
