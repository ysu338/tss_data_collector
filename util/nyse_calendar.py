from dateutil import rrule
import datetime


# Generate rule set for holiday observances on the NYSE
def nyse_holidays(a=datetime.date.today(), b=datetime.date.today() + datetime.timedelta(days=365)):
    rs = rrule.rruleset()

    # Include all potential holiday observances
    # ---- New Years Day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=12, bymonthday=31, byweekday=rrule.FR))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=1, bymonthday=1))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=1, bymonthday=2, byweekday=rrule.MO))

    # ---- MLK day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=1, byweekday=rrule.MO(3)))

    # --- Washington's Birthday
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=2, byweekday=rrule.MO(3)))

    # --- Good Friday
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, byeaster=-2))

    # --- Memorial Day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=5, byweekday=rrule.MO(-1)))

    # --- Independence Day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=7, bymonthday=3, byweekday=rrule.FR))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=7, bymonthday=4))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=7, bymonthday=5, byweekday=rrule.MO))

    # --- Labor Day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=9, byweekday=rrule.MO(1)))
    # --- Thanksgiving Day
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=11, byweekday=rrule.TH(4)))

    # --- Christmas
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=12, bymonthday=24, byweekday=rrule.FR))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=12, bymonthday=25))
    rs.rrule(rrule.rrule(rrule.YEARLY, dtstart=a, until=b, bymonth=12, bymonthday=26, byweekday=rrule.MO))

    # --- Exclude potential holidays that fall on weekends
    rs.exrule(rrule.rrule(rrule.WEEKLY, dtstart=a, until=b, byweekday=(rrule.SA, rrule.SU)))

    # --- George HW Bush Market Close Day
    rs.rdate(datetime.datetime(2018, 12, 5, 0, 0, 0))

    return rs


# --- Generate rule set for NYSE trading days
def nyse_trading_days(a=datetime.date.today(), b=datetime.date.today() + datetime.timedelta(days=365)):
    rs = rrule.rruleset()
    rs.rrule(rrule.rrule(rrule.DAILY, dtstart=a, until=b))

    # Exclude weekends and holidays
    rs.exrule(rrule.rrule(rrule.WEEKLY, dtstart=a, byweekday=(rrule.SA, rrule.SU)))
    rs.exrule(nyse_holidays(a, b))

    return rs

# Examples
# List all NYSE holiday observances for the coming year
# print "NYSE Holidays\n"
# for dy in NYSE_holidays():
#     print dy.strftime('%b %d %Y')
#
# # Count NYSE trading days in next 5 years
# print "\n\nTrading Days\n"
# for yr in range(2015,2020):
#     tdays = len(list(NYSE_tradingdays(datetime.datetime(yr,1,1),datetime.datetime(yr,12,31))))
#     print "{0}  {1}".format(yr,tdays)
