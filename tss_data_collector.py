import logging
import argparse
from config.base_config import local_data_dir
from daily_interface import dl_univ_daily, dl_tss_daily, dl_ern_daily, dl_all_daily, clear_daily_info
from intraday_interface import dl_tss_intraday, gen_intraday_tradable_univ, process_ticker_change, clear_intra_info
from intraday_interface import merge_data_to_agg


def main():
    # --------- parse command line argument ---------- #
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--freq', type=str, required=True)
    parser.add_argument('-m', '--mode', type=str, required=True)
    args = parser.parse_args()

    if args.freq == 'daily':
        dl_daily(args.mode)
    elif args.freq == 'intraday':
        dl_intraday(args.mode)
    else:
        usage()


def dl_daily(mode):
    print('Daily :')
    logging.basicConfig(filename=local_data_dir+'download_log.log', filemode='w', level=logging.INFO)
    if mode == 'univ':
        dl_univ_daily()
    elif mode == 'tss':
        dl_tss_daily()
    elif mode == 'ern':
        dl_ern_daily()
    elif mode == 'all':
        dl_all_daily()
    elif mode == 'clear':
        clear_daily_info()
    else:
        usage()


def dl_intraday(mode):
    print('Intraday :')
    logging.basicConfig(filename=local_data_dir+'download_log.log', filemode='w', level=logging.INFO)
    if mode == 'univ':
        gen_intraday_tradable_univ()
    elif mode == 'tkch':
        process_ticker_change()
    elif mode == 'tss':
        dl_tss_intraday()
    elif mode == 'merge':
        merge_data_to_agg()
    elif mode == 'clear':
        clear_intra_info()
    elif mode == 'all':
        gen_intraday_tradable_univ()
        process_ticker_change()
        dl_tss_intraday()
        merge_data_to_agg()
        clear_intra_info()
    else:
        usage()


def usage():
    usage_str = r"""
    --------------------- The program takes the following commands:----------------------------------
    Example:                                                                            
            > tss_data_collector -f daily -m univ                                                           
            > tss_data_collector -f daily -m tss                                                             
            > tss_data_collector -f daily -m ern                                                             
            > tss_data_collector -f daily -m all                                                             
            
            > tss_data_collector -f intraday -m univ                                                           
            > tss_data_collector -f intraday -m tkch                                                             
            > tss_data_collector -f intraday -m tss                                                             
            > tss_data_collector -f intraday -m merge 
            > tss_data_collector -f intraday -m clear
            > tss_data_collector -f intraday -m all                                                             

     'daily clear' : clear all data files in 'univ_name' folder and all data files in its subfolders
     'daily univ' : download univ, data saved in 'univ_name/univ' folder
     'daily tss' :  download time series data
     'daily ern' :  download earnings history and data
     'daily all':   perform steps above in the order of clear, univ, tss, ern

     'intraday univ' : generate valid universe for intraday downloading
     'intraday tkch' : check and perform the ticker changes
     'intraday tss' :  download intraday time series data
     'intraday merge' :  merge downloaded time series data with data in the "aggregated" folder
     'intraday clear' :  clear temporary information stored during intraday time series downloading
     'intraday all':   perform steps above in the order of univ, tkch, tss, merge and clear
     ----------------------------------------------------------------------------------------------
    """
    print(usage_str)


if __name__ == "__main__":
    main()
