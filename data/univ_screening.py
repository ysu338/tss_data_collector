import numpy as np
import pandas as pd
from config.external_config import daily_tss_dir, daily_univ_dir, univ_name
from features.basic_formula import ts_mean, stddev, ts_min


class UniverseScreening(object):
    """
    For tweaking the universe for trading based on a series of screen
    """
    def __init__(self, strat_name,
                 min_prc=10,
                 avg_min_volume=200000,
                 min_min_volume=150000,
                 min_dv=2000000,
                 min_num_ind=5,
                 lb_volume=20,
                 lb_hist=512,
                 lb_beta=120,
                 std_range=(0.05, 100),
                 beta_range=(0.1, 3.5),
                 cap_range=(1200, 9000000)):
        """
        :param strat_name: the specific strategy name this universe is for
        """
        self.name = strat_name
        self.min_prc = min_prc
        self.min_avg_volume = avg_min_volume
        self.min_min_volume = min_min_volume
        self.min_dv = min_dv
        self.min_num_ind = min_num_ind
        self.min_hist_count = lb_hist - 2

        self.lb_volume = lb_volume
        self.lb_hist = lb_hist
        self.lb_beta = lb_beta
        self.lb_win = np.max([self.lb_volume, self.lb_hist, self.lb_beta])

        self.std_ll, self.std_ul = std_range
        self.beta_ll, self.beta_ul = beta_range
        self.cap_ll, self.cap_ul = cap_range

    @staticmethod
    def make_year_layover_univ(date_idx, data_tks, trade_tks):
        yr_range = sorted(list(set(date_idx.year)))
        layover_univ = pd.DataFrame(False, index=date_idx, columns=data_tks)
        for yr in yr_range:
            path_str = daily_univ_dir + '/Tickers_'+univ_name+'_'+str(yr)+'.csv'
            year_tks = pd.read_csv(path_str, index_col=None, header=None, squeeze=True).values
            year_tks_valid = list(set(trade_tks) & set(year_tks))
            layover_univ.loc[str(yr), year_tks_valid] = True

        assert (layover_univ.sum(axis=1) != 0).all()

        return layover_univ

    def gen_signal_univ(self, data, univ_info, st_date='2007-01-01'):
        signal_univ = self._screen_universe_(univ_info, data)
        signal_univ = signal_univ.shift(periods=1).loc[st_date:, univ_info['trade_tks']]
        signal_univ.to_csv(daily_univ_dir + '/signal_univ_'+self.name+'_'+univ_name+'.csv')

    @staticmethod
    def load_bmk_srs():
        spy_adj = pd.read_csv(daily_tss_dir + '/SPY_PX_LAST_Adj.csv', header=None,
                              parse_dates=True, index_col=0, squeeze=True)
        spy_rtn = spy_adj / spy_adj.shift(periods=1) - 1
        return spy_rtn

    def _trim_by_industry_size_(self, signal_univ_np, date_index, trade_univ, ind_lvl_ix, data_tks):
        for d in range(len(date_index)):
            if d == 0 or date_index[d - 1].dayofweek != date_index[d].dayofweek:
                univ1 = np.where(signal_univ_np[d, :])[0]
                ind_count = trade_univ.iloc[univ1, ind_lvl_ix].value_counts()
                sm_industry = ind_count[ind_count <= self.min_num_ind - 1].index
                sm_tks = trade_univ.iloc[univ1, ind_lvl_ix][
                    trade_univ.iloc[univ1, ind_lvl_ix].isin(sm_industry)].index
                sm_tks_index = [data_tks.index(tk) for tk in sm_tks]
                signal_univ_np[d, sm_tks_index] = 0
            else:
                signal_univ_np[d, :] = signal_univ_np[d - 1, :]

        return signal_univ_np

    def _screen_universe_(self, univ_info, data):
        vum, cap = data['volume'], data['cap']
        trade_univ, trade_tks, ind_lvl_ix = univ_info['trade_univ'], univ_info['trade_tks'], univ_info['ind_lvl_ix']
        date_index, data_tks = cap.index, cap.columns.to_list()
        signal_univ = pd.DataFrame(0, index=date_index, columns=data_tks)
        roll_volume = ts_mean(data['volume'], self.lb_volume).shift(periods=0)
        roll_min_volume = ts_min(data['volume'], self.lb_volume).shift(periods=0)
        avg_dv = ts_mean(data['dv'], self.lb_volume).shift(periods=0)
        roll_price_count = data['ucl'].rolling(self.lb_hist).count().shift(periods=0)
        roll_rtn_std = ts_mean((15.6 * stddev(data['pctrtns'], 10)), 5).shift(periods=0)

        bmk_rtns = self.load_bmk_srs().loc[date_index]
        roll_beta = data['pctrtns'].rolling(self.lb_beta).cov(bmk_rtns). \
            divide(bmk_rtns.rolling(self.lb_beta).var(), axis=0)

        univ_condition1 = (data['ucl'] > self.min_prc) & \
                          (avg_dv > self.min_dv) & \
                          (roll_volume > self.min_avg_volume) & \
                          (roll_min_volume > self.min_min_volume) & \
                          (roll_price_count >= self.min_hist_count) & \
                          (roll_rtn_std > self.std_ll) & (roll_rtn_std < self.std_ul) & \
                          (roll_beta > self.beta_ll) & (roll_beta < self.beta_ul) & \
                          (data['cap'] > self.cap_ll) & (data['cap'] < self.cap_ul)

        cap1 = cap[univ_condition1]
        univ_condition = (cap1.subtract(cap1.quantile(0.1, axis=1), axis=0) > 0)

        for _, v in data.items():
            univ_condition = univ_condition & (v.notnull())

        bio_stocks = trade_univ[(trade_univ['Industry'] == 'Biotech & Pharma')].index.tolist()
        univ_condition[bio_stocks] = (univ_condition[bio_stocks]) & (data['cap'][bio_stocks] > 2500)

        yr_layover_univ = self.make_year_layover_univ(date_index, data_tks, trade_tks)
        univ_condition = univ_condition & yr_layover_univ

        signal_univ[univ_condition] = 1
        signal_univ_np = signal_univ.values

        signal_univ_np = self._trim_by_industry_size_(signal_univ_np, date_index, trade_univ, ind_lvl_ix, data_tks)
        signal_univ = pd.DataFrame(signal_univ_np, index=date_index, columns=data_tks)

        assert (signal_univ.iloc[self.lb_win + 25:, :].sum(axis=1) != 0).all()
        signal_univ[signal_univ == 0] = np.nan

        return signal_univ
