from .base_config import out_dir
from .base_config import daily_config, intraday_config
from util.io_util import gen_dir, create_daily_storage_dir, make_timezone_intra_data_dir
from.data_config import intra_start_date, intra_end_date


# ---- univ_name
univ_name = daily_config['univ_name']

# ---- daily data storage directories
daily_dir = daily_config['daily_output_dir']
daily_tss_dir, daily_univ_dir, daily_ern_dir = create_daily_storage_dir(out_dir, univ_name)

# ---- intraday strategy directories #
intra_out_dir = intraday_config['intraday_output_dir']
intra_tss_dir = intra_out_dir + 'intraday/'
intra_agg_dir = intra_tss_dir + 'aggregated/'

gen_dir(intra_tss_dir)
gen_dir(intra_agg_dir)

intra_tss_dir_utc = make_timezone_intra_data_dir(intra_tss_dir, intra_start_date, intra_end_date, tz='utc')
intra_tss_dir_local = make_timezone_intra_data_dir(intra_tss_dir, intra_start_date, intra_end_date, tz='local')
