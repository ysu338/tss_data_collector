import logging
import queue
import threading
import pandas as pd
from util.io_util import gen_dir
from data.intraday.intraday_data_univ import get_tickers_list_in_dir


def merge_data_files(src_file: str, des_file: str):
    src_data = pd.read_csv(src_file, index_col=0, header=0, parse_dates=True)
    des_data = pd.read_csv(des_file, index_col=0, header=0, parse_dates=True)
    last_des_datetime = des_data.index[-1]
    src_datetime = src_data.index
    start_index = src_datetime.searchsorted(last_des_datetime) + 1
    agg_df = pd.concat([des_data, src_data.iloc[start_index:, :]], axis=0)
    return agg_df


def intra_data_tz_convert_utc_to_east(input_data_dir_utc):
    tickers = get_tickers_list_in_dir(input_data_dir_utc)
    input_data_dir_east = input_data_dir_utc.replace("_utc", "")
    gen_dir(input_data_dir_east)

    # ---- multiprocessing begins ---- #
    to_write = queue.Queue()

    def to_csv():
        for tk0, df0 in iter(to_write.get, None):
            logging.info(f"Write {tk0} ... ")
            df0.to_csv(input_data_dir_east + tk0.replace("/", "_") + '.csv')

    def process_csv():
        for tk in tickers:
            logging.info(f"Convert tz for {tk}")
            df = pd.read_csv(input_data_dir_utc + tk.replace("/", "_") + '.csv', index_col=0, parse_dates=True)
            try:
                df.index = df.index.tz_localize('UTC').tz_convert('US/Eastern')
                df.index = df.index.tz_localize(None)
                to_write.put((tk, df))
            except AttributeError:
                continue
        to_write.put(None)

    t1 = threading.Thread(target=process_csv)
    t2 = threading.Thread(target=to_csv)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
