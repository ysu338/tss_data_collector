import pandas as pd
from data.daily.blp_interface import get_ref_data


def agg_univ_bics(group, yr_range, path):
    sec_ind_code_mapper = {'Sector': 'BI001', 'Industry': 'BI002', 'SubIndustry': 'BI003'}
    trd_uni = []

    for y in yr_range:
        ymstring = str(y)
        try:
            trd_uni_mon = pd.read_csv(path+'/Tickers_'+group+'_'+ymstring+'.csv', header=None,
                                      index_col=None, squeeze=True)
            trd_uni.append(trd_uni_mon)
        except IOError:
            pass

    agg_univ_series = pd.concat(trd_uni, axis=0).drop_duplicates()
    tks = sorted(agg_univ_series.values)
    assert len(tks) == agg_univ_series.shape[0]
    tks = list(filter(lambda x: x not in ['ZVZZT', 'ZXYZ/A'], tks))
    tickers = [tk + ' US Equity' for tk in tks]
    mega_univ_df = pd.DataFrame(data=tickers, index=tks, columns=['Tickers'])
    sec_ind_info = get_ref_data(tks, sec_ind_code_mapper.values())

    mega_univ_df['Sector'] = sec_ind_info[sec_ind_code_mapper['Sector']]
    mega_univ_df['Industry'] = sec_ind_info[sec_ind_code_mapper['Industry']]
    mega_univ_df['SubIndustry'] = sec_ind_info[sec_ind_code_mapper['SubIndustry']]

    mega_univ_df.index.name = 'Tk'
    mega_univ_df.to_csv(path + '/Tickers_' + group + '_bics.csv')


def get_univ(group, univ_dir):
    univ_df = pd.read_csv(univ_dir + '/Tickers_' + group + '_bics.csv', na_filter=False, index_col=0)
    tks = sorted(univ_df.index.tolist())
    return tks


if __name__ == "__main__":
    agg_univ_bics('MEGAU', range(2007, 2018, 1), 'Y:/Data/MEGAU/Univ')
