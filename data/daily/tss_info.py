import pandas as pd
from .blp_interface import get_bbg_hist_data
from config.external_config import univ_name
from config.data_config import daily_data_freq, adj_fields, unadj_fields


def get_hist(tickers, fields, start_date, end_date, freq, adj="FALSE", per_adj="ACTUAL"):
    tickers = sorted(tickers)
    long_tickers = [str(tk) + ' US Equity' for tk in tickers]
    datadict = get_bbg_hist_data(long_tickers, fields, start_date, end_date, freq, adj=adj, per_adj=per_adj)
    for key in datadict:
        if len(long_tickers) != datadict[key].shape[1]:
            empty_tickers = list(set(long_tickers) - set(datadict[key].columns))
            tmp_df = pd.DataFrame(0, index=datadict[key].index, columns=empty_tickers)
            datadict[key] = pd.concat([datadict[key], tmp_df], axis=1)
        datadict[key] = datadict[key].sort_index(axis=1)
        assert (datadict[key].columns == long_tickers).all()
        datadict[key].columns = tickers
        datadict[key].index = pd.DatetimeIndex(datadict[key].index)
    return datadict


def dl_single_name_hist(tks, ts_dir, start_date, end_date):
    print("... Download Single-Name Time Series Data ...")
    prc = pd.read_csv(ts_dir + '/SPY_PX_LAST_Adj.csv', index_col=0, header=None, parse_dates=True, squeeze=True)
    date_idx = prc.index.tolist()

    fields_mapper = {
        'adj': {'fields': adj_fields, 'adj_bool': 'TRUE', 'postfix': '_Adj'},
        'unadj': {'fields': unadj_fields, 'adj_bool': 'FALSE', 'postfix': '_UnAdj'}
    }

    for val in fields_mapper.values():
        if len(val['fields']) > 0:
            histdata = get_hist(tks, val['fields'], start_date, end_date, daily_data_freq, adj=val['adj_bool'])
            for key in val['fields']:
                histdata[key].loc[date_idx, :].to_csv(ts_dir+'/'+univ_name+'_'+key+val['postfix']+'.csv')

    print("... Single-Name Data Download Completed...")


def dl_spy_iwm_data(ts_dir, stdate, enddate):
    print("... Download Benchmarks Data ...")
    tks = ['SPY', 'IWM']
    fields = ['PX_OPEN', 'PX_HIGH', 'PX_LOW', 'PX_LAST']

    histdata = get_hist(tks, fields, stdate, enddate, daily_data_freq, adj='TRUE')
    unadjdata = get_hist(tks, fields, stdate, enddate, daily_data_freq, adj='FALSE')

    for key in fields:
        for t in tks:
            histdata[key][t].to_csv(ts_dir + '/' + t + '_' + key + '_Adj.csv', header=False)
            unadjdata[key][t].to_csv(ts_dir + '/' + t + '_' + key + '_UnAdj.csv', header=False)

    print("... Benchmarks Data Download Completed...")
