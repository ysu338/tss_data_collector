import numpy as np
import pandas as pd
from scipy.stats import rankdata, percentileofscore


def babs(x):
    # x can be dataframe or series
    return np.abs(x)


def log(x):
    # x can be dataframe or series
    return np.log(x)


def sign(x):
    # x can be dataframe or series
    return np.sign(x)


def rank(x):
    # cross sectional rank
    # x must be dataframe
    return x.rank(axis=1)


def delay(x, d):
    d = int(round(d))
    return x.shift(periods=d)


def correlation(x, y, d):
    d = int(round(d))
    v = x.rolling(d).corr(y)
    return v


def covariance(x, y, d):
    d = int(round(d))
    return x.rolling(d).cov(y)


def scale(x, a=1):
    return a*(x.divide(x.abs().sum(axis=1), axis=0))


def delta(x, d):
    d = int(round(d))
    return x - x.shift(periods=d)


def signedpower(x, a):
    return np.power(x, a)


def weight_sum(ses, w):
    return np.sum(ses*w)


def decay_linear(x, d):
    d = int(round(d))
    w = [float(i)/(d*(1+d)/2) for i in range(1, d+1)]
    temp2 = x.rolling(d).apply(lambda y: np.sum(y * w))
    return temp2


def ts_min(x, d):
    d = int(round(d))
    return x.rolling(d).min()


def ts_max(x, d):
    d = int(round(d))
    return x.rolling(d).max()


def bmin(x, d):
    return ts_min(x, d)


def bmax(x, d):
    return ts_max(x, d)


def ts_argmax(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: d - np.argmax(y))


def ts_argmin(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: d - np.argmin(y))


def ts_argmax_backup(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: d - pd.Series(y).idxmax())


def ts_argmin_backup(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: d - pd.Series(y).idxmin())

# def ts_rank_backup(x, d):
#     return x.rolling(d).apply(srank)
#
# def srank(array):
#     s = pd.Series(array)
#     return s.rank().values[-1] #pct=True


def ts_rank_backup1(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: rankdata(y)[-1])


def ts_rank(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: np.argsort(np.argsort(y))[-1] + 1)


def ts_rank_pct(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: float(np.argsort(np.argsort(y))[-1] + 1)/d)


def ts_rank_pct_score(x, d):
    d = int(round(d))
    return x.rolling(d).apply(lambda y: percentileofscore(y, y[-1]) / 100)


def ts_sum(x, d):
    d = int(round(d))
    return x.rolling(d).sum()


def ts_mean(x, d):
    d = int(round(d))
    return x.rolling(d).mean()


def product(x, d):
    d = int(round(d))
    return x.rolling(d).apply(np.prod)


def stddev(x, d):
    d = int(round(d))
    return x.rolling(d).std()
