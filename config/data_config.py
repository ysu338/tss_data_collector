from datetime import date
from .base_config import daily_config, intraday_config

# --- dates
today = date.today()
daily_start_date = daily_config['start_date']
daily_end_date = today.strftime("%Y%m%d") if daily_config['up_to_date'] else daily_config['end_date']
daily_year_range = range(int(daily_start_date[:4]), int(daily_end_date[:4]) + 1)
intra_start_date = intraday_config['start_date']
intra_end_date = intraday_config['end_date']


# --- daily data fields
fields_adj_unadj = daily_config['fields_adj_unadj']                  # adj and unadjusted
fields_adj = daily_config['fields_adj']                              # adjusted
fields_unadj = daily_config['fields_unadj']                          # unadjusted

unadj_fields = list(set(fields_unadj) | set(fields_adj_unadj))
adj_fields = list(set(fields_adj) | set(fields_adj_unadj))

# --- frequency options are "DAILY","WEEKLY","MONTHLY"
daily_data_freq = "DAILY"
intra_interval = intraday_config['interval']
